// function f(){
//     fetch("https://api.github.com/users/maneesh43").then(response=>response.json()).then((myjson)=>{
//         console.log(myjson);
//     })
// }




// Source: https://www.smashingmagazine.com/2018/01/deferring-lazy-loading-intersection-observer-api/

const config={
    root:'null',
    // Intersection target element default:viewport else  root: document.querySelector('#scrollArea')targets specific target
    rootMargin:"0px", 
    // Root margin :Margin around target element(top->right->bottom->left)
    threshold:0 
    // Threshhold:Percentage of intersection (0->1)

}



let observer=new IntersectionObserver((entries,self)=>{
    // Self is observer passed inside
   entries.forEach(e=>{
       if(e.isIntersecting){
           e.target.src="https://picsum.photos/200/300?random=11";
           self.unobserve(e.target);
       }
   });
},config)


const images=document.querySelectorAll('img');

images.forEach(img=>observer.observe(img))




